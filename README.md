# Terraform walk-through

What it shows?

1. Create a GitLab project
2. Add a recommended CI yml
3. Add some Terraform & update teh CI yml
   - set an environment variable within GitLab using Terraform
4. Change the environment variable in a branch
5. Open an MT to review changes
6. Merge the changes