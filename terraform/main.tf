terraform {
  backend "http" {
  }
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "~> 3.1"
    }
  }
}

variable "gitlab_access_token" {
  type = string
}

provider "gitlab" {
    token = var.gitlab_access_token
}

data "gitlab_project" "example_project" {
    id = 26474524
}

### Add a variable to the project
resource "gitlab_project_variable" "sample_project_variable" {
  key = "example_variable"
  project = data.gitlab_project.example_project.id
  value = "Greetings Master Donohoe!"
}
